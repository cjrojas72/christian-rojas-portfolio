let toggle = false;

let nav = document.querySelector(".nav");
let btnToggleNav = document.querySelector(".btn-toggle-nav");
let navItems = document.querySelector(".nav-items");

const $about = document.querySelector(".about-sec");
const $projects = document.querySelector(".project-sec");
const $resume = document.querySelector(".resume-sec");
const $contact = document.querySelector(".contact-sec");
const $home = document.querySelector(".centerH");

btnToggleNav.addEventListener("click", function() {
  if (navItems.classList.contains("active")) {
    console.log("has class");
    navItems.classList.remove("active");
  } else {
    console.log("does not have class");
    navItems.classList.add("active");
  }
});

let navFunc = function(nav) {
  if (nav === "centerH") {
    $home.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }
  if (nav === "About") {
    $about.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }
  if (nav === "Projects") {
    $projects.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }
  if (nav === "Resume") {
    $resume.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }
  if (nav === "Contact") {
    $contact.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }
};
